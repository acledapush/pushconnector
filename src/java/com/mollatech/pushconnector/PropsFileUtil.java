/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.pushconnector;

import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @author abhishekingle
 */
public class PropsFileUtil {
 
    public PropsFileUtil() {
    }

    public Properties properties = new Properties();

    public boolean LoadFile(String FilePath) {
        try {
            FileInputStream f = new FileInputStream(FilePath);
            properties.load(f);
            f.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String GetProperty(String Key) {
        try {
            String value = (String) properties.getProperty(Key);
            return value;
        } catch (Exception e) {
            return null;
        }
    }
    
}
