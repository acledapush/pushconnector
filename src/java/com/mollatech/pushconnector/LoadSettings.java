/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.pushconnector;

import java.util.Date;
import java.util.Properties;

/**
 *
 * @author abhishekingle
 */
public class LoadSettings {
    
    public static String g_strPath = null;
    public static Properties g_eSettings = null;
    
    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }        
        
        if (usrhome == null) {
            usrhome = "/Users/abhishekingle/Abhi/MultipurposeServices/apache-tomcat-8.0.24";
        }        
        usrhome += sep + "push-settings";
        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "service.conf";
        PropsFileUtil p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_eSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "service setting file failed to load >> " + filepath);
        }                        
    }
}
