/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.pushconnector;

import com.mollatech.axiom.v3.core.AxiomStatus;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "UpdateTransactionStatus", urlPatterns = {"/UpdateTransactionStatus"})
public class UpdateTransactionStatus extends HttpServlet {

    static final Logger logger = Logger.getLogger(UpdateTransactionStatus.class);
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        if(logger.getLevel() == null){
            logger.setLevel(Level.INFO);
        }
        
        logger.info("UpdateTransactionStatus is started");
        logger.info("Request servlet is #UpdateTransactionStatus at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();                        
        String channelId = LoadSettings.g_eSettings.getProperty("axiom.channelId");
        String remoteaccessId = LoadSettings.g_eSettings.getProperty("axiom.userId");
        String password = LoadSettings.g_eSettings.getProperty("axiom.password");
        logger.info("channelId :: " + channelId);
        logger.info("remoteaccessId :: " + remoteaccessId);
        logger.info("password :: " + password);        
        String transactionId = request.getParameter("transactionId");
        String integrityCheckString = request.getParameter("integrityCheckString");
        String devicepayload = request.getParameter("devicepayload");        
        logger.info("transactionId :: " + transactionId);
        logger.info("integrityCheckString :: " + integrityCheckString);
        logger.info("devicepayload :: " + devicepayload);                
        try{            
            String sessionId = new PushClient().OpenSession(channelId, remoteaccessId, password);  
            logger.info("sessionId :: " + sessionId);            
            AxiomStatus axiomResponse = new PushClient().updateTransactionStatus(sessionId, transactionId, integrityCheckString, devicepayload);
            if(axiomResponse == null){
                json.put("result", "error");
                json.put("message", "please check service server");
                json.put("resultCode", "-1");                
            }else if(axiomResponse.getErrorcode()==0){
                json.put("result", "success");
                json.put("message", axiomResponse.getError());
                json.put("resultCode", "0");                   
            }else{
                json.put("result", "error");
                json.put("message", axiomResponse.getError());
                json.put("resultCode", "-2");                  
            }        
            logger.info("ActivatePushAuthentication Response : "+json.toString());
            logger.info("ActivatePushAuthentication is ended");
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            out.print(json);
            out.flush();
            return;
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
