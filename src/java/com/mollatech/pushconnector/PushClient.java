/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.pushconnector;

import com.mollatech.axiom.v3.core.AxiomCoreV3InterfaceImplService;
import com.mollatech.axiom.v3.core.AxiomException;
import com.mollatech.axiom.v3.core.AxiomStatus;
import com.mollatech.axiom.v3.core.AxiomUser;
import com.mollatech.axiom.v3.core.Axiomv3;
import com.mollatech.axiom.v3.core.InitTransactionPackage;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.namespace.QName;

/**
 *
 * @author abhishekingle
 */
public class PushClient {
 
    private AxiomCoreV3InterfaceImplService m_service = null;
    private Axiomv3 m_port = null;
    
    public PushClient(){
        try{
            String wsdlname = LoadSettings.g_eSettings.getProperty("axiom.wsdl.name");
            String channelid = LoadSettings.g_eSettings.getProperty("axiom.channelid");           
            String remotelogin = LoadSettings.g_eSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_eSettings.getProperty("axiom.password");
            String ipAddress = LoadSettings.g_eSettings.getProperty("axiom.ipaddress");
            String strPort = LoadSettings.g_eSettings.getProperty("axiom.port");
            String strSecured = LoadSettings.g_eSettings.getProperty("axiom.secured");
            
            
            SSLContext sslContext = null;
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
                try {
                    sslContext = SSLContext.getInstance("TLS");
                } catch (NoSuchAlgorithmException ex) {
                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            } catch (KeyManagementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try { // Call Web Service Operation

                String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomCoreV3InterfaceImpl?wsdl";
                if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
                    wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomCoreV3InterfaceImpl?wsdl";
                }

                URL url = new URL(wsdlUrl);
                QName qName = new QName("http://core.v3.axiom.mollatech.com/", "AxiomCoreV3InterfaceImplService");
                                
                m_service = new AxiomCoreV3InterfaceImplService(url, qName);
                m_port = m_service.getAxiomCoreV3InterfaceImplPort();                
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            
            
            
        }catch(Exception e){
            
        }
    }
    
    public String OpenSession(String channelId, String username, String password) {
        try {
            if (channelId != null) {                
                String sessionID = m_port.openSession(channelId, username, password);
                return sessionID;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public AxiomStatus CloseSession(String sessionId) {
        try {
            if (sessionId != null) {                
                AxiomStatus status = m_port.closeSession(sessionId);
                return status;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public AxiomStatus CreateUser(String sessionId, AxiomUser user,  String tokenSerialNumber, Integer defaultTokentype){
        try{
            if (sessionId != null) {
                AxiomStatus status = m_port.createUser(sessionId, user, tokenSerialNumber, defaultTokentype);
                return status;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public AxiomStatus ResendActivationCode(String sessionId, String userId, Integer category, Integer subcategory, Integer type){
        try{
            if (sessionId != null) {
                AxiomStatus status = m_port.resendActivationCode(sessionId, userId, category, subcategory,type);
                return status;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }        
    
    public AxiomStatus ActivatePushAuthentication(String sessionId, String userId, String devicePayload, String regCode){
        try{
            if (sessionId != null) {
                AxiomStatus response = m_port.activatePushAuthentication(sessionId, userId, devicePayload, regCode);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public AxiomStatus ChangeTokenStatus(String sessionId, String userId,String tokenSerialno, Integer type, Integer status){
        try{
            if (sessionId != null) {
                AxiomStatus response = m_port.changeTokenStatus(sessionId, userId,tokenSerialno, type, status);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public AxiomStatus InitTransaction(String sessionId, String userId, InitTransactionPackage transactionPackage, String integrityCheckString){
        try{
            if (sessionId != null) {
                AxiomStatus response = m_port.initTransaction(sessionId, userId, transactionPackage, integrityCheckString);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public AxiomStatus VerifyOTP(String sessionId, String userId, String otp){
        try{
            if (sessionId != null) {
                AxiomStatus response = m_port.verifyOTP(sessionId, userId, otp);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus getTransactionStatus(String sessionId, String txId, String integrityCheckString) {        
        try{
            if (sessionId != null) {
                AxiomStatus response = m_port.getTransactionStatus(sessionId, txId, integrityCheckString);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus resetUser(String sessionid, String userid, Integer type) {
        try{
            if (sessionid != null) {
                AxiomStatus response = m_port.resetUser(sessionid, userid, type);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus deleteUser(String sessionid, String userid) {
        try{
            if (sessionid != null) {
                AxiomStatus response = m_port.deleteUser(sessionid, userid);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public AxiomUser getUserBy(String sessionid, int type, String searchFor) throws AxiomException {
        try{
            if (sessionid != null) {
                AxiomUser response = m_port.getUserBy(sessionid, type, searchFor);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus changeUserDetails(String sessionid, AxiomUser axiomuser, int defaultTokenType) {
        try{
            if (sessionid != null) {
                AxiomStatus response = m_port.changeUserDetails(sessionid,axiomuser, defaultTokenType);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus updateTransactionStatus(java.lang.String sessionid, java.lang.String transactionId, java.lang.String integrityCheckString, java.lang.String devicepayload) {                
        try{
            if (sessionid != null) {
                AxiomStatus response = m_port.updateTransactionStatus(sessionid, transactionId, integrityCheckString, devicepayload);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }           

    public AxiomStatus validateRegistrationCode(java.lang.String sessionid, java.lang.String userid, java.lang.String regCode, java.lang.String deviceid, java.lang.Integer tokenType) throws AxiomException {
        try{
            if (sessionid != null) {
                AxiomStatus response = m_port.validateRegistrationCode(sessionid, userid, regCode, deviceid, tokenType);
                return response;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }                
    }
}
