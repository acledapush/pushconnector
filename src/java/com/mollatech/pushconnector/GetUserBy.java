/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.pushconnector;

import com.mollatech.axiom.nucleus.db.Trusteddevice;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement;
import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
import com.mollatech.axiom.v3.core.AxiomUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "GetUserBy", urlPatterns = {"/GetUserBy"})
public class GetUserBy extends HttpServlet {

    static final Logger logger = Logger.getLogger(GetUserBy.class);
    private int SearchByUserid = 1;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.INFO);
        }
        logger.info("GetUserBy is started");
        logger.info("Request servlet is #GetUserBy at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();                        
        String channelId = LoadSettings.g_eSettings.getProperty("axiom.channelId");
        String remoteaccessId = LoadSettings.g_eSettings.getProperty("axiom.userId");
        String password = LoadSettings.g_eSettings.getProperty("axiom.password");
        logger.info("channelId :: " + channelId);
        logger.info("remoteaccessId :: " + remoteaccessId);
        logger.info("password :: " + password);        
        String userid = request.getParameter("userid");
        String devicePayload = request.getParameter("devicePayload");
        logger.info("userid :: " + userid);
        logger.info("devicePayload :: " + devicePayload);
        try{
            String sessionId = new PushClient().OpenSession(channelId, remoteaccessId, password);  
            logger.info("sessionId :: " + sessionId);            
            AxiomUser axiomUser = new PushClient().getUserBy(sessionId, SearchByUserid, userid);
            if(axiomUser == null){
                json.put("result", "error");
                json.put("message", "user not found");
                json.put("resultCode", "-1");                
            }else{
                //check for trust setting
                int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.MOBILETRUST_SETTING;
                int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
                Object settingsObj = new SettingsManagement().getSetting(sessionId, channelId, iType, iPreference);
                boolean isDeviceTrackingEnable = false;
                if(settingsObj != null){
                    MobileTrustSettings mobileObj = (MobileTrustSettings) settingsObj;
                    isDeviceTrackingEnable = mobileObj.bDeviceTracking;                
                }
                Trusteddevice[] td = new TrustDeviceManagement().getTrusteddevices(sessionId, channelId, axiomUser.getUserId());
                // If device tracking enable
                if(td != null && isDeviceTrackingEnable){
                    json.put("result", "error");
                    json.put("message", "Your Push token already registered with other device");
                    json.put("resultCode", "-2"); 
                    return;
                }else if(td != null && !isDeviceTrackingEnable){
                // If device tracking disbale
                    JSONObject deviceDetails = new JSONObject(devicePayload);
                    String requestedDeviceID = ""; boolean isDeviceIdPresent = false;
                    if(deviceDetails.has("_deviceId")){
                        requestedDeviceID = deviceDetails.getString("_deviceId");
                    }                     
                    for(int i=0; i<td.length; i++){
                        String presentDeviceId = td[i].getDeviceid();
                        if(requestedDeviceID.equals(presentDeviceId)){
                            isDeviceIdPresent = true;
                            break;
                        }
                    }
                    if(isDeviceIdPresent){
                        json.put("result", "error");
                        json.put("message", "Your Push token already registered with other device");
                        json.put("resultCode", "-2"); 
                        return;
                    }
                }
                
                json.put("result", "success");
                json.put("message", "user find");
                json.put("resultCode", "0");
                json.put("phoneNumber", axiomUser.getPhoneNo());
                json.put("emailId", axiomUser.getEmail());
            }
            logger.info("GetUserBy Response : "+json.toString());
            logger.info("GetUserBy ended");
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            out.print(json);
            out.flush();
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
