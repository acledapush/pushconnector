/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.pushconnector;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.PropertyConfigurator;
/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "Log4JInitServlet", urlPatterns = {"/Log4JInitServlet"}, loadOnStartup = 1)
public class Log4JInitServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    
     @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext sc = config.getServletContext();
        String webAppPath = sc.getContextPath();
        webAppPath = webAppPath.replace("/", "");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = System.getenv("DOMAIN_HOME");
        }
        String ErrorLevel_PushConnector = "INFO";        
        Properties logProp = new Properties();
        logProp.put("log4j.debug", "TRUE");
        logProp.put("log4j.appender.file", "org.apache.log4j.RollingFileAppender");
        logProp.put("log4j.rootLogger", ErrorLevel_PushConnector + ", RollingAppender");
        logProp.put("log4j.appender.RollingAppender", "org.apache.log4j.DailyRollingFileAppender");
        logProp.put("log4j.appender.RollingAppender.File", usrhome + "/PushServiceConnectorLogs/" + webAppPath + "/logs.log");
        logProp.put("log4j.appender.RollingAppender.DatePattern", "'.'yyyy-MM-dd");
        logProp.put("log4j.appender.RollingAppender.layout", "org.apache.log4j.PatternLayout");
        logProp.put("log4j.appender.RollingAppender.layout.ConversionPattern", "[%p] %d %c %M - %m%n");
        logProp.put("log4j.appender.file.MaxFileSize", "100MB");
        logProp.put("log4j.appender.file.MaxBackupIndex", "10");
        PropertyConfigurator.configure(logProp);
        super.init(config);
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
